# Gitlab

![Снимок экрана от 2023-02-15 23-21-39](https://user-images.githubusercontent.com/75438030/219145139-0caaea31-7c79-46a6-a1d2-56129ff5171d.png)

GitLab — веб-инструмент жизненного цикла DevOps с открытым исходным кодом, представляющий систему управления репозиториями кода для Git с собственной вики, системой отслеживания ошибок, CI/CD пайплайном и другими функциями.

### Запуск

```
git clone https://gitlab.com/alexnet-public/ansible.git
cd ansible
ansible-playbook -i hosts gitlab/gitlab.yml
```
