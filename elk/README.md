# ELK

### Сменить пароль для elasticsearch

```

# Password reset
/usr/share/elasticsearch/bin/elasticsearch-reset-password -u elastic

# Elastik test
curl --cacert /etc/elasticsearch/certs/http_ca.crt -u elastic https://localhost:9200
curl -k --user elastic:'ktNBdiTxO3hKqASVvHyu' https://127.0.0.1:9200

```

### Сгенерируем токен для Kibana


```

# Сгенерируем токен для Kibana
/usr/share/elasticsearch/bin/elasticsearch-create-enrollment-token -s kibana

```

![Снимок экрана от 2023-01-21 18-10-26](https://user-images.githubusercontent.com/75438030/213873248-c2b608eb-1bc9-4eb6-9cde-89fe65dcd0ba.png)

### Сгенерируем pincode для Kibana

```


# Сгенерируем PINCODE для Kibana
/usr/share/kibana/bin/kibana-verification-code

```


![Снимок экрана от 2023-01-21 18-18-20](https://user-images.githubusercontent.com/75438030/213873587-b5910e48-2acf-48ad-9932-0b9196f06a17.png)

### Принципиальная схема

---

![Снимок экрана от 2023-01-21 01-01-19](https://user-images.githubusercontent.com/75438030/213813082-d4c7fd18-a202-4e34-bf68-8b6cd242697c.png)

---

### Nginx -> logstash -> elastic (Конфиг)

---

`Запуск logstash`

```
 /usr/share/logstash/bin/logstash -f /etc/logstash/conf.d/logstash.conf

```

`Конфиг`

```
root@fhm3fm29b31j1l14ov0c:~# cat /etc/logstash/conf.d/logstash.conf
input {
  file {
  path => "/var/log/nginx/access.log"
  start_position => "beginning"
  }
}
filter {
  grok {
   match => { "message" => "%{IPORHOST:remote_ip} - %{DATA:user_name}
\[%{HTTPDATE:access_time}\] \"%{WORD:http_method} %{DATA:url}
HTTP/%{NUMBER:http_version}\" %{NUMBER:response_code} %{NUMBER:body_sent_bytes}
\"%{DATA:referrer}\" \"%{DATA:agent}\"" }
  }
  mutate {
  remove_field => [ "host" ]
  }
}
output {
  elasticsearch { 
    data_stream => "true"
    hosts => ["https://localhost:9200"]
    ssl => true
    ssl_certificate_verification => false
    user => elastic
    password => "2NGIX+vxhIVBMkDbcyQb"
  }
}


```
---
### Nginx -> filebeat -> elastic 7

filebeat modules enable nginx

```

/etc/filebeat/modules.d/nginx.yml
# Access logs
access:
  enabled: true

```

---

`Ссылки:`

https://serveradmin.ru/ustanovka-i-nastroyka-elasticsearch-logstash-kibana-elk-stack/

https://hackmd.io/@oleg-zhulin/rk_PKO7zi#923-%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-Logstash

https://www.dmosk.ru/instruktions.php?object=elk-ubuntu

