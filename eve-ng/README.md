# Eve-ng - стенд ( Ubuntu 20.04  Selectel )

![Снимок экрана от 2023-04-11 03-21-59](https://user-images.githubusercontent.com/75438030/231001519-18ba882b-1b86-4c20-9887-733170f80813.png)


Репозиторий, содержащий Ansible плейбук для автоматизированной установки и настройки EVE-NG на удаленном сервере Selectel. Плейбук содержит шаги для установки всех необходимых зависимостей, загрузки и установки EVE-NG, а также настройки сетевых интерфейсов и сервисов.

Данный репозиторий позволяет автоматизировать установку и настройку EVE-NG на удаленном сервере Selectel, что значительно упрощает процесс установки и экономит время на ручной настройке.

### Установка  (15-20 мин)

Создадим виртуальную машину на базе ОС Ubuntu 20.04 (*обязательно к выполнению*)

Заходим на виртуалку и меняем пароль root (*обязательно к выполнению*)

```
passwd root
```

Первая команда git clone https://gitlab.com/alexnet-public/ansible.git клонирует репозиторий Ansible, расположенный на GitLab, на локальный компьютер. Клонирование репозитория необходимо для того, чтобы получить плейбуки и другие файлы, необходимые для управления нашими целевыми системами.


```
git clone https://gitlab.com/alexnet-public/ansible.git
```


Команда cd ansible переходит в директорию ansible, куда были склонированы файлы из репозитория.


```
cd ansible 
```

Файл hosts, который открывается командой vim hosts, содержит информацию о хостах, на которых мы будем запускать наши плейбуки. В файле мы можем указать IP-адреса, доменные имена, SSH-порты и другие параметры хостов. Этот файл используется в качестве источника данных для модуля inventory в Ansible.

```
vim hosts
```

Команда ansible-playbook -i hosts eve-ng/eve-ng.yml запускает плейбук eve-ng.yml, который находится в директории eve-ng, используя информацию об инвентаризации хостов из файла hosts. В данном случае, плейбук eve-ng.yml содержит инструкции для установки и настройки EVE-NG на целевой системе.

```
ansible-playbook -i hosts eve-ng/eve-ng.yml
```

Переходим в Веб-консоль  Selectel и логинимся под root


![Снимок экрана от 2023-04-11 04-36-11](https://user-images.githubusercontent.com/75438030/231011959-a49cc0ea-77d3-47e2-a154-aaa765bf1566.png)

Вводим измененный пароль root

![Снимок экрана от 2023-04-11 03-25-58](https://user-images.githubusercontent.com/75438030/231002925-35c985d5-4af9-4b12-a129-810f7b8a9f64.png)

Enter

![Снимок экрана от 2023-04-11 03-26-18](https://user-images.githubusercontent.com/75438030/231002940-2c068f83-7d62-4550-bfed-e3ceac87fda4.png)

Enter

![Снимок экрана от 2023-04-11 03-26-28](https://user-images.githubusercontent.com/75438030/231002968-d27686b1-0aeb-442a-b372-1dedba1f03db.png)

IP static

![Снимок экрана от 2023-04-11 01-58-04](https://user-images.githubusercontent.com/75438030/231003055-086dc922-84f1-4712-98c1-7fc2cb258703.png)

Enter

![Снимок экрана от 2023-04-11 03-38-02](https://user-images.githubusercontent.com/75438030/231004272-648a8e29-9c04-4f04-9a6c-b7de8f334f13.png)

Enter

![Снимок экрана от 2023-04-11 03-38-09](https://user-images.githubusercontent.com/75438030/231004284-54fa158a-fd2b-4acb-a51c-4843eb439dcc.png)

Enter

![Снимок экрана от 2023-04-11 03-38-17](https://user-images.githubusercontent.com/75438030/231004295-94e5b633-e7bc-42ad-998b-cebefc3bd42c.png)

Enter

![Снимок экрана от 2023-04-11 03-26-40](https://user-images.githubusercontent.com/75438030/231003000-c4569b80-d941-4225-8802-bfe81eea1832.png)

Enter

![Снимок экрана от 2023-04-11 03-26-51](https://user-images.githubusercontent.com/75438030/231003018-249a3dda-1c58-406f-b816-1677d0fd96d5.png)

После перезагрузки создадим фаил настроек сети

`
nano /etc/netplan/01-netcfg.yaml
`
```
network:
  version: 2
  renderer: networkd
  ethernets:
    eth0:
      dhcp4: no
  bridges:
    br0:
      interfaces:
        - eth0
      dhcp4: no
      addresses:
        - 192.168.0.2/24
      gateway4: 192.168.0.1
      nameservers:
        addresses:
          - 188.93.16.19
          - 188.93.17.19
```
Запустим службу systemd-networkd.service

```
systemctl enable systemd-networkd.service
systemctl start systemd-networkd.service
systemctl status systemd-networkd.service

```
Применим настройки сети

```
netplan apply
```
Также отключим авто апгрейд ОС

```
vim /etc/apt/apt.conf.d/20auto-upgrades

APT::Periodic::Update-Package-Lists "0";
APT::Periodic::Unattended-Upgrade "0";

```


Перхоим к Веб-интерфейсу   (login: admin | passwd: eve)

```
http://ipaddress/#/login
```
![Снимок экрана от 2023-04-11 06-00-27](https://user-images.githubusercontent.com/75438030/231021676-ee52017c-ceeb-4700-acb9-9edf2ca55c18.png)

# Включение ВАВ 

https://wiki.astralinux.ru/pages/viewpage.action?pageId=43615491

Включение ВАВ на хост-машине
Временное включение без перезагрузки ОС, изменения остаются в силе до перезагрузки ОС:
Остановить все запущенные виртуальные машины;

Выгрузить модуль виртуализации:

`Intel`

```
modprobe -r kvm_intel
```
`AMD`

```
modprobe  -r kvm_amd
```

Загрузить модуль виртуализации с включенной ВАВ:

`Intel`
```
modprobe kvm_intel nested=1
```
`AMD`
```
modprobe kvm_amd nested=1
```

Постоянное включение, автоматическое включение после перезагрузки ОС:

Отредактировать (при отсутствии - создать) файл /etc/modprobe.d/kvm.conf, включив в него следующую строчку:

`Intel`

```
options kvm_intel nested=1
```
`AMD`

```
options kvm_amd nested=1
```

Перезагрузить машину:

```
reboot
```

#### Также можно добавить nested в crontab


```
crontab -e
```
Добавим строчку

```
@reboot /usr/sbin/modprobe kvm_amd nested=1

```
Перезагрузить машину:

```
reboot
```

# FRR (alpine qemu linix)

```
cd /opt/unetlab/addons/qemu/
mkdir linux-alpine
chmod 777 linux-alpine
cd linux-alpine
wget http://dl-cdn.alpinelinux.org/alpine/v3.11/releases/x86_64/alpine-standard-3.11.3-x86_64.iso
mv alpine-standard-3.11.3-x86_64.iso cdrom.iso
/opt/unetlab/wrappers/unl_wrapper -a fixpermissions
```
