# Mysql MASTER-SLAVE  - (Тестовый стенд)

![Снимок экрана от 2023-02-15 18-32-43](https://user-images.githubusercontent.com/75438030/219074387-bcd09940-a124-45c8-9966-6e7f0c4632e8.png)

MySql Master-Slave — это настройка репликации между двумя или более серверами MySql, где один сервер (главный) служит основным источником данных, а другие серверы (ведомые) реплицируют данные с главного. Эта настройка обеспечивает высокую доступность и масштабируемость, поскольку ведомые устройства могут использоваться для разгрузки запросов на чтение от ведущего устройства и обеспечения избыточности в случае сбоя.

Чтобы настроить репликацию MySql Master-Slave, вам необходимо настроить главный сервер, чтобы разрешить репликацию, создать учетную запись пользователя для подчиненного устройства для подключения к главному, а затем настроить подчиненный сервер для репликации с главного.

Вы можете легко настроить репликацию MySql Master-Slave с помощью Docker. Для этого вам нужно создать два контейнера Docker — один для ведущего и один для ведомого, а затем настроить репликацию между ними.

`Заливаем плейбук`

```
ansible-playbook -i hosts mysql-master-slave-test.yml

```

`Копируем тестовую БД`

```
git clone https://gitlab.com/alexnet-public/ansible.git
cd ansible/mysql/test_db/sakila-db/

```

`Смотрим id контейнеров`

```
docker ps

CONTAINER ID   IMAGE       COMMAND                  CREATED          STATUS          PORTS                 NAMES
0ea1392e0629   mysql:8.0   "docker-entrypoint.s…"   59 minutes ago   Up 59 minutes   3306/tcp, 33060/tcp   mysql-slave
32519228192c   mysql:8.0   "docker-entrypoint.s…"   59 minutes ago   Up 59 minutes   3306/tcp, 33060/tcp   mysql-master


```
`Смотрим ip адреса контейнеров`

```
root@mysql:~# docker network ls
NETWORK ID     NAME          DRIVER    SCOPE
ae5c219ddf3c   bridge        bridge    local
441d83bb2d01   host          host      local
b331574af4cf   none          null      local
63113551f827   replication   bridge    local


root@mysql:~# docker container inspect -f '{{ .NetworkSettings.Networks.replication.IPAddress }}' mysql-master
172.19.0.2

root@mysql:~# docker container inspect -f '{{ .NetworkSettings.Networks.replication.IPAddress }}' mysql-slave
172.19.0.3


```

`Заливаем базу`
```
# master =>
mysql -u root -p"qwertyX14" -h 172.19.0.2 sakila < test_db/sakila-db/sakila-schema.sql 
mysql -u root -p"qwertyX14" -h 172.19.0.2 sakila < test_db/sakila-db/sakila-data.sql 

# slave =>
mysql -u root -p"qwertyX14" -h 172.19.0.3 sakila < test_db/sakila-db/sakila-schema.sql 
mysql -u root -p"qwertyX14" -h 172.19.0.3 sakila < test_db/sakila-db/sakila-data.sql 

```
`Смотрим (File | Position) master сервера`

```
# master =>
mysql -u root -p"qwertyX14" -h 172.19.0.2 -e "SHOW MASTER STATUS;"

Пример:
+------------------+----------+--------------+------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+------------------+----------+--------------+------------------+-------------------+
| mysql-bin.000003 |  1360099 | sakila       |                  |                   |
+------------------+----------+--------------+------------------+-------------------+

```
`Следующим шагом требуется прописать в базе данных на сервер
slave, кто является master и данные полученные в File и Position:`
```
# slave =>
mysql -u root -p"qwertyX14" -h 172.19.0.3 -e "CHANGE MASTER TO MASTER_HOST='172.19.0.2',
MASTER_USER='root', MASTER_PASSWORD='qwertyX14', MASTER_LOG_FILE='mysql-bin.000003',MASTER_LOG_POS=1360099;"
```
`Далее запускаем журнал ретрансляции, и проверим статус
операций:`
```
mysql -u root -p"qwertyX14" -h 172.19.0.3 -e "START SLAVE;"
mysql -u root -p"qwertyX14" -h 172.19.0.3 -e "SHOW SLAVE STATUS\G"
```
---

# Проверка кластера


```
root@mysql:~# docker ps
CONTAINER ID   IMAGE       COMMAND                  CREATED       STATUS       PORTS                 NAMES
0ea1392e0629   mysql:8.0   "docker-entrypoint.s…"   3 hours ago   Up 3 hours   3306/tcp, 33060/tcp   mysql-slave
32519228192c   mysql:8.0   "docker-entrypoint.s…"   3 hours ago   Up 3 hours   3306/tcp, 33060/tcp   mysql-master
```
`Смотрис ip контейнера`
```
root@mysql:~# docker container inspect -f '{{ .NetworkSettings.Networks.replication.IPAddress }}' mysql-master
172.19.0.2

root@mysql:~# docker container inspect -f '{{ .NetworkSettings.Networks.replication.IPAddress }}' mysql-slave
172.19.0.3
```
`Делаем тестовый SQL запрос на master`
```
root@mysql:~# mysql -u root -p"qwertyX14" -h 172.19.0.2 -e "USE sakila; SELECT * FROM actor ORDER BY actor_id DESC LIMIT 5;"
```
![Снимок экрана от 2023-02-15 20-41-36](https://user-images.githubusercontent.com/75438030/219109806-d0ad2a4d-5d89-401c-a262-a3c72fd60e7c.png)

`Делаем тестовый SQL запрос на slave`
```
root@mysql:~# mysql -u root -p"qwertyX14" -h 172.19.0.3 -e "USE sakila; SELECT * FROM actor ORDER BY actor_id DESC LIMIT 5;"
```
![Снимок экрана от 2023-02-15 20-41-58](https://user-images.githubusercontent.com/75438030/219109880-f8506c83-3e29-4ea2-a3c3-232576171381.png)

`Добавим запись в БД master`
```
mysql -u root -p"qwertyX14" -h 172.19.0.2 -e "USE sakila; INSERT INTO actor (first_name, last_name) VALUES ('ALEX', 'VAKHRAMEEV');"

```
`Проверка синхронизации master-slave`

![Снимок экрана от 2023-02-15 20-45-11](https://user-images.githubusercontent.com/75438030/219110609-a9a2f758-0c91-4721-86a0-ecf2ef5a911c.png)

`mysql -u root -p"qwertyX14" -h 172.19.0.3 -e "SHOW SLAVE STATUS\G"`
![Снимок экрана от 2023-02-15 20-47-00](https://user-images.githubusercontent.com/75438030/219111128-fdffffbb-547a-47e0-83d0-765130dcedec.png)

