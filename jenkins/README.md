# Jenkins

![Снимок экрана от 2023-02-15 23-41-25](https://user-images.githubusercontent.com/75438030/219151076-7fb4a7a6-750b-4f6f-8770-0a80ffbacf01.png)

Jenkins — программная система с открытым исходным кодом на Java, предназначенная для обеспечения процесса непрерывной интеграции программного обеспечения. Ответвлена в 2008 году от проекта Hudson, принадлежащего компании Oracle, основным его автором — Косукэ Кавагути. Распространяется под лицензией MIT.

### Запуск

```
git clone https://gitlab.com/alexnet-public/ansible.git
cd ansible
ansible-playbook -i hosts jenkins/jenkins.yml
```
