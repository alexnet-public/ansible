# Prometheus Node Exporter

![Снимок экрана от 2023-02-15 23-58-35](https://user-images.githubusercontent.com/75438030/219158138-bbd05273-27cd-4495-98c9-0a5d87138f3d.png)

Prometheus Node Exporter — это экспортер для Prometheus, который собирает метрики с заданного узла. Он используется для мониторинга показателей системного уровня, таких как использование памяти, использование диска, загрузка ЦП и других показателей системного уровня. Prometheus Node Exporter можно установить и настроить на сервере который мы будем мониторить.Node Exporter собирает метрики и отправляет их в Prometheus. Затем Prometheus может использовать эти показатели для создания предупреждений и визуализации их на информационных панелях Grafana.

### Запуск

```
git clone https://gitlab.com/alexnet-public/ansible.git
cd ansible
ansible-playbook -i hosts node-exporter/node-exporter.yaml

```
