# PostgreSQL

![Снимок экрана от 2023-02-16 00-22-27](https://user-images.githubusercontent.com/75438030/219169866-8b92ee60-8af9-436f-84c4-a7cfd868a984.png)

PostgreSQL — свободная объектно-реляционная система управления базами данных. Существует в реализациях для множества UNIX-подобных платформ, включая AIX, различные BSD-системы, HP-UX, IRIX, Linux, macOS, Solaris/OpenSolaris, Tru64, QNX, а также для Microsoft Windows.

### Запуск

```
git clone https://gitlab.com/alexnet-public/ansible.git
cd ansible
ansible-playbook -i hosts postgresql/pgsql.yml 

```
