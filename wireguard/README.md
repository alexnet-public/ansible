# Wireguard (Тестовый стенд)
![Снимок экрана от 2023-02-16 16-43-13](https://user-images.githubusercontent.com/75438030/219381008-922efffa-b114-48f3-9a46-a4893f69af11.png)

WireGuard® - это очень простой и одновременно быстрый и современный VPN, использующий актуальную криптографию. Он предназначен для того, чтобы быть быстрее, проще, легче и полезнее, чем IPsec, избегая при этом массивных проблем. Он предполагает быть значительно более производительным, чем OpenVPN. 

### Запуск

```
git clone https://gitlab.com/alexnet-public/ansible.git
cd ansible
ansible-playbook -i hosts wireguard/wireguard.yml
```
Как только отработает playbook появится QR-code.

`Сканируем код с телефона`

![Снимок экрана от 2023-02-16 15-51-57](https://user-images.githubusercontent.com/75438030/219381038-4f5b04b5-073a-4db6-a06b-ea759b2a0f8e.png)
