# Grafana

![Снимок экрана от 2023-02-15 23-38-49](https://user-images.githubusercontent.com/75438030/219149789-7757c2de-5dda-464e-8f50-da327cbb61b4.png)

Grafana — это платформа визуализации и мониторинга данных с открытым исходным кодом. Он предоставляет настраиваемую панель мониторинга для отображения и анализа показателей из широкого спектра источников данных. Grafana позволяет пользователям быстро выявлять тенденции и аномалии в своих данных, а также помогает им более глубоко исследовать и понимать свои данные. Grafana также предоставляет мощные возможности оповещения и уведомления, позволяя пользователям получать уведомления при выполнении определенных условий. Благодаря интуитивно понятному пользовательскому интерфейсу и широкому набору функций Grafana является идеальным инструментом для мониторинга и анализа данных.

### Запуск

```
git clone https://gitlab.com/alexnet-public/ansible.git
cd ansible
ansible-playbook -i hosts grafana/grafana.yml
```
