# Zabbix

![Снимок экрана от 2023-02-16 17-02-02](https://user-images.githubusercontent.com/75438030/219385628-b0a17748-3bd2-4e75-81cb-9c6fefc64978.png)

Zabbix - это открытая система мониторинга инфраструктуры, которая позволяет отслеживать и анализировать производительность ИТ-среды. Она поддерживает инструменты для извлечения данных из произвольных источников, таких как базы данных, приложения, серверы, сети и другие устройства. Zabbix предлагает мощные механизмы оповещения, отображения данных и дает возможность мониторинга на разных уровнях гибкости. 

### Запуск

```
git clone https://gitlab.com/alexnet-public/ansible.git
cd ansible
ansible-playbook -i hosts zabbix/zabbix.yml

```
